import 'dart:convert';

import 'package:laundry_delivery_app/data/entity/DryTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WaterTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WeightPrice.dart';

OrderDetail orderDetailFromJson(String str) =>
    OrderDetail.fromJson(json.decode(str));

String orderDetailToJson(OrderDetail data) => json.encode(data.toJson());

class OrderDetail {
  int basketNo;
  //List<dynamic> weightPrices;
  //List<dynamic> waterTempPrices;
  // List<dynamic> dryTempPrices;
  WeightPrice weightPrice;
  WaterTempPrice waterTempPrice;
  DryTempPrice dryTempPrice;

  String note;

  OrderDetail(
      {this.basketNo,
      this.weightPrice,
      this.waterTempPrice,
      this.dryTempPrice,
      this.note});

  OrderDetail.fromJson(Map<String, dynamic> json) {
    basketNo = json['basket_no'];
    weightPrice = json['weight_price'];
    waterTempPrice = json['water_temp_price'];
    dryTempPrice = json['dry_temp_price'];
    // if (json['weight_price'] != null) {
    //   weightPrices = new List<WeightPrice>();
    //   json['weight_price'].forEach((v) {
    //     weightPrices.add(new WeightPrice.fromJson(v));
    //   });
    // }
    // if (json['water_temp_price'] != null) {
    //   waterTempPrices = new List<WaterTempPrice>();
    //   json['water_temp_price'].forEach((v) {
    //     waterTempPrices.add(new WaterTempPrice.fromJson(v));
    //   });
    // }
    // if (json['dry_temp_price'] != null) {
    //   dryTempPrices = new List<DryTempPrice>();
    //   json['dry_temp_price'].forEach((v) {
    //     dryTempPrices.add(new DryTempPrice.fromJson(v));
    //   });
    // }
    note = json['note'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basket_no'] = this.basketNo;
    data['weight_price'] = this.weightPrice;
    data['water_temp_price'] = this.waterTempPrice;
    data['dry_temp_price'] = this.dryTempPrice;
    // if (this.weightPrices != null) {
    //   data['weight_price'] = this.weightPrices.map((v) => v.toJson()).toList();
    // }
    // if (this.waterTempPrices != null) {
    //   data['water_temp_price'] =
    //       this.waterTempPrices.map((v) => v.toJson()).toList();
    // }
    // if (this.dryTempPrices != null) {
    //   data['dry_temp_price'] =
    //       this.dryTempPrices.map((v) => v.toJson()).toList();
    // }
    data['note'] = this.note;
    return data;
  }
}
