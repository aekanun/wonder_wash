class Authen {
  int code;
  String msgEn;
  String msgTh;
  String token;
  bool register;
  int user_Id;
  int customer_id;
  String phone_number;

  Authen(
      {this.code,
      this.msgEn,
      this.msgTh,
      this.token,
      this.register,
      this.user_Id,
      this.customer_id,
      this.phone_number});

//------------------------------------------ false
//  {code: 0, msg_en: Plesae register, msg_th: กรุณาลงทะเบียน, token: null, register: false, user_id: null}

//------------------------------------------ success
//  {token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6MiwidXNlcl9jdXN0b21lcl9pZCI6IiIsImN1c3RvbWVyX25hbWUiOiJ0ZXN0MiIsImlhdCI6MTYyMzIyODE4OCwiZXhwIjoxNjIzMzE0NTg4fQ.w7NZRPlny2xySOhQmOCkLZJzAfH_7cd-aCHREP-pQhQ,
//code: 0, msg_en: Login success, msg_th: เข้าระบบสำเร็จ, register: true, customer_id: 2, phone_number: 0811231234}

  Authen.fromJson(Map<String, dynamic> json) {
    print("json data: " + json.toString());
    code = json['code'];
    msgEn = json['msg_en'];
    msgTh = json['msg_th'];
    token = json['token'];
    register = json['register'];
    user_Id = json['user_id'];
    if (json['customer_id'] != null) {
      customer_id = json["customer_id"];
    }
    if (json['phone_number'] != null) {
      phone_number = json['phone_number'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['msg_en'] = this.msgEn;
    data['msg_th'] = this.msgTh;
    data['token'] = this.token;
    data['register'] = this.register;
    data['user_id'] = this.user_Id;
    data['customer_id'] = this.customer_id;
    data['phone_number'] = this.phone_number;
    return data;
  }

  void clear() {
    code = 0;
    msgEn = "";
    msgTh = "";
    token = "";
    register = false;
    user_Id = 0;
    customer_id = 0;
    phone_number = "";
  }
}
