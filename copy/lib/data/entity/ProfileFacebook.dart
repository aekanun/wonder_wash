class ProfileFacebook {
  String name;
  String firstName;
  String lastName;
  String email;
  String id;
  String picture_url;

  ProfileFacebook(
      {this.name,
      this.firstName,
      this.lastName,
      this.email,
      this.id,
      this.picture_url});

  ProfileFacebook.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['id'] = this.id;
    return data;
  }

  void setPictureUrl(String picture) {
    this.picture_url = picture;
  }

  void clear() {
    name = "";
    firstName = "";
    lastName = "";
    email = "";
    id = "";
    picture_url = "";
  }
}
