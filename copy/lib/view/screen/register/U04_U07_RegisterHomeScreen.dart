import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/RegisterViewModel.dart';
import 'package:provider/provider.dart';

import 'U04_UserRegisterScreen.dart';
import 'U05_VerifyPhoneNumberScreen.dart';
import 'U06_OtpVerificationCodeScreen.dart';
import 'U07_IntroScreen.dart';

class RegisterHomeScreen extends StatefulWidget {
  String currentNavigator;
  RegisterHomeScreen({this.currentNavigator});
  @override
  _RegisterHomeState createState() => _RegisterHomeState();
}

class _RegisterHomeState extends State<RegisterHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => RegisterViewModel.instance(),
      child: Consumer<RegisterViewModel>(builder: (context, register, _) {
        if (register.dto.currentNavigation == u05) {
          return VerifyPhoneNumberScreen();
        } else if (register.dto.currentNavigation == u06) {
          return OtpVerificationCodeScreen();
        } else if (register.dto.currentNavigation == u07) {
          register.setDefaultCurrentnavigation();
          return IntroScreen();
        } else {
          if (widget.currentNavigator != null) {
            print('RegisterHomeScreen currentNavigator: ' +
                widget.currentNavigator);
            return VerifyPhoneNumberScreen();
          } else {
            return UserRegisterScreen();
          }
        }
      }),
    );
  }
}
