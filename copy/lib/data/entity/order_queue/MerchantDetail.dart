import 'Location.dart';

class MerchantDetail {
  int merchantId;
  String name;
  Location location;
  String phone;

  MerchantDetail({this.merchantId, this.name, this.location, this.phone});

  MerchantDetail.fromJson(Map<String, dynamic> json) {
    merchantId = json['merchant_id'];
    name = json['name'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_id'] = this.merchantId;
    data['name'] = this.name;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    data['phone'] = this.phone;
    return data;
  }
}