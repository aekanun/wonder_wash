import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/Router.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:provider/provider.dart';

import 'utility/ContainsService.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        // ChangeNotifierProvider(
        //   create: (context) => PermissionLocationViewmodel.instance(),
        // ),
        // ChangeNotifierProvider(
        //   create: (context) => AuthenViewModel.instance(),
        // ),
        // ChangeNotifierProvider(
        //   create: (context) => RegisterViewModel.instance(),
        // ),
        // ChangeNotifierProvider(
        //   create: (context) => LaundryStoreViewModel.instance(),
        // ),
        ChangeNotifierProvider(
          create: (context) => OrderViewModel.instance(),
        ),
        ChangeNotifierProvider(
          create: (context) => GoogleMapViewModel.instance(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrderHistoryViewModel.instance(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrderQueueViewModel.instance(),
        ),
        ChangeNotifierProvider(
          create: (context) => SettingViewModel.instance(),
        ),
      ],
      child: MyHomePage(),
      // child: EasyLocalization(
      //     child: MyHomePage(),
      //     supportedLocales: [
      //       Locale('en', 'US'),
      //       Locale('th', 'TH'),
      //     ],
      //     path: 'lib/lang/langs.csv',
      //     assetLoader: CsvAssetLoader())
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Laundry delivery app',
      theme: ThemeData(
        primarySwatch: Colors.white,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Routers.generateRoute,
      initialRoute: u01,
    );
  }
}
