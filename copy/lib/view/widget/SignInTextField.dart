import 'package:flutter/material.dart';

class SignInTextField{

  SignInTextField();

  Widget entryField(BuildContext context,String title, TextEditingController textEditController,
      FocusNode focusNode, FocusNode requestFocusNode,
      {bool isPassword = false, bool isAutoFocus = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 5,
          ),
          TextField(
            autofocus: isAutoFocus,
            focusNode: focusNode,
            controller: textEditController,
            obscureText: isPassword,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            onSubmitted: (term) {
              focusNode.unfocus();
              FocusScope.of(context).requestFocus(requestFocusNode);
            },
          )
        ],
      ),
    );
  }
}