import 'package:after_layout/after_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexScreen extends StatefulWidget {
  IndexScreenState createState() => IndexScreenState();
}

class IndexScreenState extends State<IndexScreen>
    with AfterLayoutMixin<IndexScreen> {
  @override
  void initState() {
    super.initState();
    print(
        '==============================IndexScreen init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================IndexScreen dispose==============================');
    super.dispose();
  }

  Future getPermisstion() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = (sharedPreferences.getString('accessToken'));
    print('token================================= $token');
    await Future.delayed(Duration(seconds: 3), () {
      if (token != null) {
        Navigator.pushReplacementNamed(context, u08_0);
      } else {
        Navigator.pushReplacementNamed(context, u02);
      }
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    getPermisstion();
  }

  Future<bool> onWillPop() {
    //Although, exit(0) also works but the app closes abruptly and doesn't look nice, kind of seems that the app has crashed.
    //exit(0);
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: CupertinoPageScaffold(
            child: Center(
          child: CupertinoActivityIndicator(
            radius: 10,
          ),
        )),
        onWillPop: onWillPop);
  }
}
