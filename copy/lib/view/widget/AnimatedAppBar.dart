import 'package:flutter/material.dart';

class AnimatedAppBar extends StatelessWidget {
  AnimationController colorAnimationController;
  Animation colorTween, homeTween, workOutTween, iconTween, drawerTween;
  Function onPressed;

  AnimatedAppBar({
    @required this.colorAnimationController,
    @required this.onPressed,
    @required this.colorTween,
    @required this.homeTween,
    @required this.iconTween,
    @required this.drawerTween,
    @required this.workOutTween,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      child: AnimatedBuilder(
        animation: colorAnimationController,
        builder: (context, child) => AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.home,
              color: drawerTween.value,
            ),
            onPressed: onPressed,
          ),
          backgroundColor: colorTween.value,
          elevation: 0,
          titleSpacing: 0.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.network(
                  'http://i2.wp.com/www.thaismescenter.com/wp-content/uploads/2020/10/1102.jpg',
                  height: 50,
                  width: 50),
            ],
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(7),
              child: Icon(
                Icons.person,
                color: iconTween.value,
              ),
            ),

            // Padding(
            //   padding: const EdgeInsets.all(7),
            //   child: CircleAvatar(
            //     backgroundImage:
            //         NetworkImage('image_url'),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
