class WaterTempPrice {
  String temp;
  int price;

  WaterTempPrice({this.temp, this.price});

  WaterTempPrice.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['price'] = this.price;
    return data;
  }
}
