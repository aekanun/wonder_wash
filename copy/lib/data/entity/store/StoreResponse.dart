import 'Store.dart';

class StoreResponse {
  int code;
  String msgEn;
  String msgTh;
  List<Store> store;
  List<dynamic> advertise;
  int pendingOrder;

  StoreResponse({this.code, this.msgEn, this.msgTh, this.store});

  StoreResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msgEn = json['msg_en'];
    msgTh = json['msg_th'];
    pendingOrder = json['pending_order'];
    if (json['advertise'] != null) {
      
      advertise = new List<dynamic>();
      json['advertise'].forEach((v) {
        advertise.add(v);
      });
    }
    if (json['store'] != null) {
      store = new List<Store>();
      json['store'].forEach((v) {
        store.add(new Store.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['msg_en'] = this.msgEn;
    data['msg_th'] = this.msgTh;
    data['pending_order'] = this.pendingOrder;
    if (this.store != null) {
      data['advertise'] = this.advertise.map((v) => v.toJson()).toList();
    }
    if (this.store != null) {
      data['store'] = this.store.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
