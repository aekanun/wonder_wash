import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/model/OrderModel.dart';
import 'package:laundry_delivery_app/view/screen/order/Attr.dart';

class OrderViewModel with ChangeNotifier {
  OrderModel _orderModel;
  DtoOrder dto;
  Attr attr = new Attr();
  int selectedRadioWeightPrice = 0;
  int selectedRadioDryTempPrice = 0;
  int selectedRadioWaterTemp = 0;
  bool selectedCheckBoxImmediately = false;

  OrderViewModel();
  OrderViewModel.instance() {
    this._orderModel = OrderModel.instance;
    this.dto = new DtoOrder();
    this.attr.title = "Add order";
    this.attr.scfdRootKey = new GlobalKey<ScaffoldState>();
    this.attr.animeListKey = new GlobalKey<AnimatedListState>();

    //this.dto.store = this._orderModel.getStore;
  }

  void getStore() {
    this.dto.store = this._orderModel.getStore;
    //notifyListeners();
  }

  Future<void> confirmOrder() async {
    this._orderModel.confirmOrder(this);
  }

  get getSelectedCheckBoxImmediately {
    return this.selectedCheckBoxImmediately;
  }

  void updateSelectedCheckBoxImmediately(bool value) {
    print('selectedCheckBox: $value');
    this.selectedCheckBoxImmediately = value;
    if (this.selectedCheckBoxImmediately) {
      setDateTimeOrder(DateTime.now());
    }
    notifyListeners();
  }

  get getSelectedRadioWaterTemp {
    return this.selectedRadioWaterTemp;
  }

  void updateSelectedRadioWaterTemp(int selectedRadio) {
    print('selectWaterTemp: $selectedRadio');
    this.selectedRadioWaterTemp = selectedRadio;
    notifyListeners();
  }

  get getSelectedRadioWeightPrice {
    return this.selectedRadioWeightPrice;
  }

  void updateSelectedRadioWeightPrice(int selectedRadio) {
    print('selectWeightPrice: $selectedRadio');
    this.selectedRadioWeightPrice = selectedRadio;
    notifyListeners();
  }

  get getSelectedRadioDryTempPrice {
    return this.selectedRadioDryTempPrice;
  }

  void updateSelectedRadioDryTempPrice(int selectedRadio) {
    print('selectDry: $selectedRadio');
    this.selectedRadioDryTempPrice = selectedRadio;
    notifyListeners();
  }

  void setDto(DtoOrder dto) {
    this.selectedRadioWeightPrice = 0;
    this.selectedRadioDryTempPrice = 0;
    this.selectedRadioWaterTemp = 0;
    this.dto = dto;
    notifyListeners();
  }

  void clearOrder() {
    this._orderModel.clearOrder();
  }

  void clearOrderSubmit() {
    this._orderModel.clearOrderSubmit();
  }

  void removeItem(int basketNo, int charge) {
    this._orderModel.removeItemByIndex(this, basketNo, charge);
    notifyListeners();
  }

  void setPaymentMethod(int paymentMethod) {
    this._orderModel.setPaymentMethod(this, paymentMethod);
  }

  void setDateTimeOrder(DateTime dateTime) {
    this._orderModel.setDateTime(this, dateTime);
  }

  void setOnConfirmOrder(TextEditingController noteController) {
    this._orderModel.setOrderList(
        this,
        this.selectedRadioWeightPrice,
        this.selectedRadioWaterTemp,
        this.selectedRadioDryTempPrice,
        noteController.text);
  }
}
