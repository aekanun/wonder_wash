import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoTutorialScreen extends StatefulWidget {
  VideoTutorialScreenState createState() => VideoTutorialScreenState();
}

class VideoTutorialScreenState extends State<VideoTutorialScreen> {
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  YoutubePlayerController _controller;
  //TextEditingController _idController;
  // TextEditingController _seekToController;

  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  double _volume = 100;
  bool _muted = false;
  bool _isPlayerReady = false;

  final List<String> _ids = [
    'vH3EhJFY3us',
    'gQDByCdjUXw',
  ];

  @override
  void initState() {
    super.initState();
    print(
        '==============================VideoTutorial init==============================');
    _controller = YoutubePlayerController(
      initialVideoId: _ids.first,
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ),
    )..addListener(listener);
    //  _idController = TextEditingController();
    // _seekToController = TextEditingController();
    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;
  }

  @override
  void dispose() {
    _controller.dispose();
    //  _idController.dispose();
    //  _seekToController.dispose();
    print(
        '==============================VideoTutorial dispose==============================');
    super.dispose();
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerBuilder(
      onExitFullScreen: () {
        // The player forces portraitUp after exiting fullscreen. This overrides the behaviour.
        SystemChrome.setPreferredOrientations(DeviceOrientation.values);
      },
      player: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        progressIndicatorColor: Colors.blueAccent,
        onReady: () {
          _isPlayerReady = true;
        },
        onEnded: (data) {
          // _controller
          //     .load(_ids[(_ids.indexOf(data.videoId) + 1) % _ids.length]);
          // _showSnackBar('Next Video Started!');
          _controller.pause();
          Navigator.pop(context);
        },
      ),
      builder: (context, player) => Scaffold(
        // key: _scaffoldKey,
        // appBar: AppBar(
        //   leading: Padding(
        //     padding: const EdgeInsets.only(left: 12.0),
        //     child: Image.asset(
        //       'assets/ypf.png',
        //       fit: BoxFit.fitWidth,
        //     ),
        //   ),
        //   title: const Text(
        //     'Tutorial',
        //     style: TextStyle(color: Colors.white),
        //   ),
        // ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 36),
                padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          "Tutorial",
                          style:
                              TextStyle(fontSize: 20.0, color: Colors.purple),
                        ))
                  ],
                ),
              ),
              player,
              InkWell(
                child: Container(
                  alignment: Alignment.center,
                  constraints: BoxConstraints.expand(height: 45),
                  child: Text("Back",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18, color: Colors.purple)),
                  margin: EdgeInsets.only(bottom: 36),
                ),
                onTap: () {
                  //ปุ่มย้อนหลับมีเงื่อนไข การใช้งานครั้งแรก เข้าจากหน้า intro หรือเป็นสสมาชิกอยู่แล้ว เข้าจากหน้า setting
                  _controller.pause();
                  // Navigator.pushNamed(context, u07);
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
