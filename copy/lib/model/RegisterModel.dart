import 'dart:convert';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:laundry_delivery_app/data/entity/Authen.dart';
import 'package:laundry_delivery_app/data/entity/FacebookResponse.dart';
import 'package:laundry_delivery_app/data/entity/GoogleResponse.dart';
import 'package:laundry_delivery_app/model/services/service.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/viewmodel/ViewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterModel {
  static GoogleSignIn googleSignIn; //Can be used both platform
  static FacebookLogin facebookSignIn;
  DtoRegister _dto;
  Service service;

  SharedPreferences _prefs;

  RegisterModel._privateConstructor() {
    googleSignIn = GoogleSignIn(clientId: google_client_id);
    facebookSignIn = new FacebookLogin();
    this._dto = new DtoRegister();
    service = new Service();
    SharedPreferences.getInstance().then((value) {
      _prefs = value;
    });
  }

  static final RegisterModel instance = RegisterModel._privateConstructor();

  void registerFromSocialLogin(String authenServer, String token) {
    this._dto.authenServer = authenServer;
    this._dto.authen.token = token;
  }

  Future<void> socialRegister(
      {RegisterViewModel registerViewModel, String phoneNumber}) async {
    this._dto.phoneNumber = phoneNumber;
    dynamic body = jsonEncode(<String, String>{
      'authen_server': this._dto.authenServer,
      'access_token': this._dto.authen.token,
      'phone_no': this._dto.phoneNumber,
    });
    print("===========socialRegister body: " + body.toString());
    this._dto.dataEntry = false;
    this._dto.authen =
        Authen.fromJson(await service.postAuthen(url_customer_register, body));
    print(body.toString());
    if (this._dto.authen != null) {
      if (this._dto.authen.code == 0) {
        this._dto.dataEntry = true;
        await _prefs.setString("accessToken", this._dto.authen.token);
        this._dto.errorMessage = "register success";
        registerViewModel.setDto(this._dto);
      }
    }
  }

// ----------------------------user----------------------------
  setRegisterData(Map mapController, RegisterViewModel registerViewModel) {
    this._dto.email = mapController['email'];
    this._dto.password = mapController['password'];
    this._dto.confirmPassword = mapController['confirm_password'];
    this._dto.phoneNumber = mapController['phone'];
    this._dto.dataEntry = false;
    if (this._dto.email.isNotEmpty &&
        this._dto.password.isNotEmpty &&
        this._dto.confirmPassword.isNotEmpty &&
        this._dto.phoneNumber.isNotEmpty) {
      this._dto.dataEntry = true;
      customerRegister(registerViewModel);
    }
    registerViewModel.setDto(this._dto);
  }

  Future<void> customerRegister(RegisterViewModel registerViewModel) async {
    dynamic body = jsonEncode(<String, String>{
      'authen_server': 'api',
      'user_name': this._dto.email,
      'password': this._dto.password,
      'phone_no': this._dto.phoneNumber,
    });
    print("===========customerRegister body: " + body.toString());
    this._dto.authen =
        Authen.fromJson(await service.postAuthen(url_customer_register, body));
    if (this._dto.authen != null) {
      if (this._dto.authen.code == 0) {
        await _prefs.setString("accessToken", this._dto.authen.token);
        this._dto.errorMessage = "register success";
        registerViewModel.setDto(this._dto);
      }
    }
  }

  setOtp(Map mapController, RegisterViewModel registerViewModel) {
    this._dto.otp = mapController['otp'];
    if (this._dto.otp.isNotEmpty) {
      customerVerify(registerViewModel);
    } else {
      registerViewModel.setDto(this._dto);
    }
  }

  Future<void> customerVerify(RegisterViewModel registerViewModel) async {
    dynamic body = jsonEncode(<String, String>{
      'otp': this._dto.otp,
    });
    print("===========customerVerify body: " + body.toString());
    dynamic response = await service.postAuthen(url_customer_verify, body);
    print(response.toString());
    if (response != null) {
      if (response['code'] == 0) {
        this._dto.errorMessage = "verify success";
        registerViewModel.setDto(this._dto);
      }
    }
  }

  void updateNavigation(
      String navigation, RegisterViewModel registerViewModel) {
    this._dto.currentNavigation = navigation;
    registerViewModel.updateNavigation(this._dto);
  }

  void setDefaultCurrentnavigation() {
    this._dto.currentNavigation = '';
  }
}

class DtoRegister {
  String errorMessage;
  String email;
  String password;
  String confirmPassword;
  String phoneNumber;
  String otp;
  bool dataEntry = false;
  Authen authen = new Authen();
  String authenServer;
  // FacebookResponse facebookResponse;
  // GoogleResponse googleResponse;

  String currentNavigation = "";

  DtoRegister();
}
