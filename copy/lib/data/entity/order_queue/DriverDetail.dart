class DriverDetail {
  String name1;
  String phone1;
  String name2;
  String phone2;
  String orderFrom;
  String orderTo;
  String distance;
  String moneyPickup;
  String moneyReceive;

  DriverDetail(
      {this.name1,
      this.phone1,
      this.name2,
      this.phone2,
      this.orderFrom,
      this.orderTo,
      this.distance,
      this.moneyPickup,
      this.moneyReceive});

  DriverDetail.fromJson(Map<String, dynamic> json) {
    name1 = json['name_1'];
    phone1 = json['phone_1'];
    name2 = json['name_2'];
    phone2 = json['phone_2'];
    orderFrom = json['order_from'];
    orderTo = json['order_to'];
    distance = json['distance'];
    moneyPickup = json['money_pickup'];
    moneyReceive = json['money_receive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name_1'] = this.name1;
    data['phone_1'] = this.phone1;
    data['name_2'] = this.name2;
    data['phone_2'] = this.phone2;
    data['order_from'] = this.orderFrom;
    data['order_to'] = this.orderTo;
    data['distance'] = this.distance;
    data['money_pickup'] = this.moneyPickup;
    data['money_receive'] = this.moneyReceive;
    return data;
  }
}