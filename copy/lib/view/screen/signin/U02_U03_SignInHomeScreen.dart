import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/view/screen/laundry_home/FindLocationScreen.dart';
import 'package:laundry_delivery_app/view/screen/laundry_home/U08_LaundryHomeStoreScreen.dart';
import 'package:laundry_delivery_app/view/screen/register/U04_U07_RegisterHomeScreen.dart';
import 'package:laundry_delivery_app/view/screen/register/U05_VerifyPhoneNumberScreen.dart';
import 'package:laundry_delivery_app/view/screen/signin/U03_UserSignInScreen.dart';
import 'package:laundry_delivery_app/viewmodel/AuthenViewModel.dart';
import 'package:provider/provider.dart';

import 'U02_SignInScreen.dart';

class SignInHomeScreen extends StatefulWidget {
  SignInHomeState createState() => SignInHomeState();
}

class SignInHomeState extends State<SignInHomeScreen> {
  @override
  void initState() {
    super.initState();
    // AuthenViewModel permission =
    //     Provider.of<AuthenViewModel>(context, listen: false);
    // permission.initViewModel();
    print(
        '==============================SignInHome init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================SignInHome dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(
        '==============================SignInHome BuildContext==============================');

    return ChangeNotifierProvider(
      create: (context) => AuthenViewModel.instance(),
      child: Consumer<AuthenViewModel>(
          // key: AuthenViewModel().consumerAuthenKey,
          builder: (context, authen, _) {
        print(authen.dto.currentNavigation);
        if (authen.dto.currentNavigation == u03) {
          return UserSignInScreen();
        } else if (authen.dto.currentNavigation == u08) {
          return LaundryHomeStoreScreen();
        } else if (authen.dto.currentNavigation == u08_0) {
          return FindLocationScreen();
        } else if (authen.dto.currentNavigation == u04) {
          return RegisterHomeScreen(currentNavigator: u05);
        } else {
          return SignInScreen();
        }
      }),
    );
    // return Consumer<AuthenViewModel>(
    //     // key: AuthenViewModel().consumerAuthenKey,
    //     builder: (context, authen, _) {
    //   print(authen.dto.currentNavigation);
    //   if (authen.dto.currentNavigation == u03) {
    //     return UserSignInScreen();
    //   } else if (authen.dto.currentNavigation == u08) {
    //     return LaundryHomeStoreScreen();
    //   } else if (authen.dto.currentNavigation == u08_0) {
    //     return FindLocationScreen();
    //   } else if (authen.dto.currentNavigation == u05) {
    //     return VerifyPhoneNumberScreen();
    //   } else {
    //     return SignInScreen();
    //   }
    // });
  }
}
