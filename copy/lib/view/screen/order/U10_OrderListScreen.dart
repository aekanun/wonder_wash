import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/styles/StyleConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderViewModel.dart';
import 'package:provider/provider.dart';

class OrderListScreen extends StatefulWidget {
  OrderListScreenStateless createState() => OrderListScreenStateless();
}

class OrderListScreenStateless extends State<OrderListScreen> {
  TextEditingController noteController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    OrderViewModel orderViewModel =
        Provider.of<OrderViewModel>(context, listen: false);
    orderViewModel.getStore();
    print(
        '==============================OrderList init==============================');
  }

  @override
  void dispose() {
    print(
        '==============================OrderList dispose==============================');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderViewModel>(builder: (context, viewModel, _) {
      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          // key: attr.scfdRootKey,
          // iconTheme: IconThemeData(
          //   color: Colors.black, //change your color here
          // ),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () => Navigator.pushNamed(context, u11),
          ),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: Stack(
          children: <Widget>[
            Container(
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 0, left: 18, right: 16),
                        child: Text(
                          viewModel.dto.store != null
                              ? viewModel.dto.store.shop_name
                              : '',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                            color: AppTheme.darkerText,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, bottom: 8, top: 8),
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Text(
                              viewModel.dto.store != null
                                  ? viewModel.dto.store.address
                                  : '',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14,
                                color: AppTheme.nearyPurple,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, bottom: 8, top: 8),
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Text(
                              "Step 1: Pick up weight of clothes",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14,
                                color: AppTheme.nearyPurple,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              //      controller: ,
                              itemCount: viewModel.dto.store != null
                                  ? viewModel.dto.store.weightPrice.length
                                  : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return RadioListTile(
                                    title: Text(viewModel.dto.store
                                            .weightPrice[index].weight
                                            .toString() +
                                        " kg."),
                                    secondary: Text(viewModel
                                            .dto.store.weightPrice[index].price
                                            .toString() +
                                        " ฿"),
                                    value: index,
                                    groupValue:
                                        viewModel.getSelectedRadioWeightPrice,
                                    onChanged: (val) {
                                      viewModel
                                          .updateSelectedRadioWeightPrice(val);
                                    });
                              },
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, bottom: 8, top: 8),
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Text(
                              "Step 2: Water temperature",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14,
                                color: AppTheme.nearyPurple,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              //      controller: ,
                              itemCount: viewModel.dto.store != null
                                  ? viewModel.dto.store.waterTempPrice.length
                                  : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return RadioListTile(
                                    title: Text(viewModel
                                        .dto.store.waterTempPrice[index].temp),
                                    subtitle: Text('-°C'),
                                    secondary: Text(viewModel.dto.store
                                            .waterTempPrice[index].price
                                            .toString() +
                                        " ฿"),
                                    value: index,
                                    groupValue:
                                        viewModel.getSelectedRadioWaterTemp,
                                    onChanged: (val) {
                                      viewModel
                                          .updateSelectedRadioWaterTemp(val);
                                    });
                              },
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, bottom: 8, top: 8),
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Text(
                              "Step 3: Dryer temperature",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14,
                                color: AppTheme.nearyPurple,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              //      controller: ,
                              itemCount: viewModel.dto.store != null
                                  ? viewModel.dto.store.dryTempPrice.length
                                  : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return RadioListTile(
                                    title: Text(viewModel
                                        .dto.store.dryTempPrice[index].temp),
                                    secondary: Text(viewModel.dto.store
                                            .dryTempPrice[index].price
                                            .toString() +
                                        " ฿"),
                                    value: index,
                                    groupValue:
                                        viewModel.getSelectedRadioDryTempPrice,
                                    onChanged: (val) {
                                      viewModel
                                          .updateSelectedRadioDryTempPrice(
                                              val);
                                    });
                              },
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, bottom: 16, right: 16, top: 16),
                        child: TextField(
                          maxLines: 3,
                          controller: noteController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            hintText: 'Note',
                            hintStyle: TextStyle(
                              height: 2.8,
                            ),
                            prefixIcon: Icon(Icons.chrome_reader_mode),
                            fillColor: Colors.grey[200],
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, bottom: 16, right: 16, top: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new GestureDetector(
                              onTap: () => Navigator.pushNamed(context, u11),
                              child: Container(
                                width: 48,
                                height: 48,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: AppTheme.nearlyWhite,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(16.0),
                                    ),
                                    border: Border.all(
                                        color: AppTheme.grey.withOpacity(0.2)),
                                  ),
                                  child: Icon(
                                    Icons.close,
                                    color: AppTheme.nearlyRed,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Expanded(
                                child: new GestureDetector(
                              onTap: () {
                                viewModel.setOnConfirmOrder(noteController);
                                Navigator.pushNamed(context, u11);
                              },
                              child: Container(
                                height: 48.0,
                                padding: new EdgeInsets.all(8.0),
                                alignment: Alignment.center,
                                decoration: TextStyleConst.linearGradient(),
                                child: Text(
                                  'Confirm',
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white),
                                ),
                              ),
                            ))
                          ],
                        ),
                      ),
                    ]),
              ),
            ),
          ],
        ),
      );
    });
  }
}
