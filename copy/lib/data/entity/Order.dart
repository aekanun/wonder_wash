import 'order/CustomerLocation.dart';
import 'order/OrderDetail.dart';
import 'dart:convert';

Order orderFromJson(String str) => Order.fromJson(json.decode(str));

String orderToJson(Order data) => json.encode(data.toJson());

class Order {
  String customerAddress;
  int merchantId;
  String pickupTime;
  CustomerLocation customerLocation;
  List<OrderDetail> orderDetail;
  int totalCharge;
  int paymentOption;

  Order(
      {this.customerAddress,
      this.merchantId,
      this.pickupTime,
      this.customerLocation,
      this.orderDetail,
      this.totalCharge,
      this.paymentOption});

  Order.fromJson(Map<String, dynamic> json) {
    customerAddress = json['customer_address'];
    merchantId = json['merchant_id'];
    pickupTime = json['pickup_time'];
    customerLocation = json['customer_location'] != null
        ? new CustomerLocation.fromJson(json['customer_location'])
        : null;
    if (json['order_detail'] != null) {
      orderDetail = new List<OrderDetail>();
      json['order_detail'].forEach((v) {
        orderDetail.add(new OrderDetail.fromJson(v));
      });
    }
    totalCharge = json['total_charge'];
    paymentOption = json['payment_option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customer_address'] = this.customerAddress;
    data['merchant_id'] = this.merchantId;
    data['pickup_time'] = this.pickupTime;
    if (this.customerLocation != null) {
      data['customer_location'] = this.customerLocation.toJson();
    }
    if (this.orderDetail != null) {
      data['order_detail'] = this.orderDetail.map((v) => v.toJson()).toList();
    }
    data['total_charge'] = this.totalCharge;
    data['payment_option'] = this.paymentOption;

    return data;
  }
}
