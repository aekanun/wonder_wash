import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/view/styles/StyleConst.dart';
import 'package:laundry_delivery_app/view/widget/SignInButton.dart';
import 'package:laundry_delivery_app/view/widget/SignInTextField.dart';
import 'package:laundry_delivery_app/viewmodel/RegisterViewModel.dart';
import 'package:provider/provider.dart';

class UserRegisterScreen extends StatefulWidget {
  UserRegisterScreenState createState() => UserRegisterScreenState();
}

class UserRegisterScreenState extends State<UserRegisterScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();
  TextEditingController phoneNumberController = new TextEditingController();

  var mapController = new Map();

  FocusNode _focusNodeEmail;
  FocusNode _focusNodePassword;
  FocusNode _focusNodeConfirmPassword;
  FocusNode _focusNodePhoneNumber;
  FocusNode _focusNodeSubmit;

  @override
  initState() {
    super.initState();
    //Provider.of<RegisterViewModel>(context, listen: false);
    _focusNodeEmail = FocusNode();
    _focusNodePassword = FocusNode();
    _focusNodeConfirmPassword = FocusNode();
    _focusNodePhoneNumber = FocusNode();
    _focusNodeSubmit = FocusNode();
    print('UserRegisterScreen initState');
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    phoneNumberController.dispose();
    _focusNodeEmail.dispose();
    _focusNodePassword.dispose();
    _focusNodeConfirmPassword.dispose();
    _focusNodePhoneNumber.dispose();
    _focusNodeSubmit.dispose();
    print('UserRegisterScreen dispose');
    super.dispose();
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        SignInTextField().entryField(context, "Email", emailController,
            _focusNodeEmail, _focusNodePassword,
            isAutoFocus: true),
        SignInTextField().entryField(context, "Password", passwordController,
            _focusNodePassword, _focusNodeConfirmPassword,
            isPassword: true),
        SignInTextField().entryField(
            context,
            "Confirm password",
            confirmPasswordController,
            _focusNodeConfirmPassword,
            _focusNodePhoneNumber,
            isPassword: true),
        SignInTextField().entryField(context, "Phone number",
            phoneNumberController, _focusNodePhoneNumber, _focusNodeSubmit),
      ],
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'W',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Color(0xffe46b10),
          ),
          children: [
            TextSpan(
              text: 'el',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'come',
              style: TextStyle(color: Color(0xffe46b10), fontSize: 30),
            ),
          ]),
    );
  }

  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text('or'),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget buildButtonSignIn(RegisterViewModel registerViewModel) {
    return InkWell(
      focusNode: _focusNodeSubmit,
      child: Container(
        margin: EdgeInsets.only(top: 12),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: TextStyleConst.linearGradient(),
        child: Text(
          'Confirm',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      onTap: () {
        mapController['email'] = emailController.text;
        mapController['password'] = passwordController.text;
        mapController['confirm_password'] = confirmPasswordController.text;
        mapController['phone'] = phoneNumberController.text;
        registerViewModel.setRegisterData(mapController);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final registerViewModel = Provider.of<RegisterViewModel>(context);
    return Scaffold(
        body: Container(
      height: height,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: height * .1),
                  _title(),
                  // SizedBox(height: 50),
                  // InkWell(
                  //   onTap: registerViewModel.loginWithFacebook,
                  //   child: SignInButton().signInButton(
                  //       "F",
                  //       "Sign in with Facebook",
                  //       Color(0xff1959a9),
                  //       Color(0xff2872ba)),
                  // ),
                  // SizedBox(height: 5),
                  // InkWell(
                  //   onTap: registerViewModel.loginWithGmail,
                  //   child: SignInButton().signInButton(
                  //       "G",
                  //       "Sign in with G-mail",
                  //       Color(0xffEA4335),
                  //       Color(0xffee6a5d)),
                  // ),
                  _divider(),
                  _emailPasswordWidget(),
                  buildButtonSignIn(registerViewModel),
                  SizedBox(height: height * .055),
                  InkWell(
                    child: Container(
                      alignment: Alignment.center,
                      constraints: BoxConstraints.expand(height: 45),
                      child: Text("Back",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 18, color: Colors.purple)),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, u02);
                    },
                  ),
                ],
              ),
            ),
          ),
          // Positioned(top: 40, left: 0, child: _backButton()),
        ],
      ),
    ));
  }
}
