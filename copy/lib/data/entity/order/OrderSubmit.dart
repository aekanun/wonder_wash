import 'Data.dart';

class OrderSubmit {
  int code;
  String msgEn;
  String msgTh;
  Data data;

  OrderSubmit({this.code, this.msgEn, this.msgTh, this.data});

  OrderSubmit.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msgEn = json['msg_en'];
    msgTh = json['msg_th'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['msg_en'] = this.msgEn;
    data['msg_th'] = this.msgTh;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  void clear() {
    this.code = 0;
    this.msgEn = '';
    this.msgTh = '';
    this.data.clear();
  }
}
