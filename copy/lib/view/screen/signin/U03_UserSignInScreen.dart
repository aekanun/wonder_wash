import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/view/styles/StyleConst.dart';
import 'package:laundry_delivery_app/view/widget/SignInTextField.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:provider/provider.dart';

class UserSignInScreen extends StatefulWidget {
  UserSignInScreenState createState() => UserSignInScreenState();
}

class UserSignInScreenState extends State<UserSignInScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  var mapController = new Map();
  FocusNode _focusNodeEmail;
  FocusNode _focusNodePassword;
  FocusNode _focusNodeSubmit;

  @override
  void initState() {
    super.initState();
    _focusNodeEmail = new FocusNode();
    _focusNodePassword = new FocusNode();
    _focusNodeSubmit = new FocusNode();
    print(
        '==============================UserSignIn init==============================');
  }

  @override
  void dispose() {
    _focusNodeEmail.dispose();
    _focusNodePassword.dispose();
    print(
        '==============================UserSignIn dispose==============================');
    super.dispose();
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        SignInTextField().entryField(context, "Email", emailController,
            _focusNodeEmail, _focusNodePassword,
            isAutoFocus: true),
        SignInTextField().entryField(context, "Password", passwordController,
            _focusNodePassword, _focusNodeSubmit,
            isPassword: true),
      ],
    );
  }

  Container buildTextFieldEmail() {
    return Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.grey, borderRadius: BorderRadius.circular(16)),
        child: TextField(
          autofocus: true,
          focusNode: _focusNodeEmail,
          decoration: InputDecoration.collapsed(hintText: "Email"),
          controller: emailController,
          style: TextStyle(fontSize: 18),
          onSubmitted: (term) {
            _focusNodeEmail.unfocus();
            FocusScope.of(context).requestFocus(_focusNodePassword);
          },
        ));
  }

  Container buildTextFieldPassword() {
    return Container(
      padding: EdgeInsets.all(12),
      margin: EdgeInsets.only(top: 12),
      decoration: BoxDecoration(
          color: Colors.grey, borderRadius: BorderRadius.circular(16)),
      child: TextField(
        obscureText: true,
        focusNode: _focusNodePassword,
        decoration: InputDecoration.collapsed(hintText: "Password"),
        controller: passwordController,
        style: TextStyle(fontSize: 18),
        onSubmitted: (term) {
          _focusNodePassword.unfocus();
          FocusScope.of(context).requestFocus(_focusNodeSubmit);
        },
      ),
    );
  }

  Widget buildButtonSignIn(AuthenViewModel authen) {
    return InkWell(
      focusNode: _focusNodeSubmit,
      child: Container(
        height: 48.0,
        padding: new EdgeInsets.all(8.0),
        alignment: Alignment.center,
        decoration: TextStyleConst.linearGradient(),
        child: Text(
          'Login',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      onTap: () {
        mapController['email'] = emailController.text;
        mapController['password'] = passwordController.text;
        authen.loginWithUser(mapController);
      },
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'W',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Color(0xffe46b10),
          ),
          children: [
            TextSpan(
              text: 'el',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'come',
              style: TextStyle(color: Color(0xffe46b10), fontSize: 30),
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        height: height,
        child: Stack(
          children: <Widget>[
            Consumer<AuthenViewModel>(
              builder: (context, authen, _) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(height: height * .1),
                        _title(),
                        _emailPasswordWidget(),
                        buildButtonSignIn(authen),
                        SizedBox(height: height * .055),
                        InkWell(
                          child: Container(
                            alignment: Alignment.center,
                            height: 48.0,
                            child: Text("Back",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18, color: Colors.purple)),
                          ),
                          onTap: () {
                            //Navigator.pushNamed(context, u02);
                            authen.setNavigation(u02);
                          },
                        ),
                      ],
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
