import 'package:intl/intl.dart';
import 'package:laundry_delivery_app/data/entity/DryTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WaterTempPrice.dart';
import 'package:laundry_delivery_app/data/entity/WeightPrice.dart';

import 'Location.dart';

class Store {
  int merchant_Id;
  String shop_name;
  String address;
  Location location;
  List<dynamic> workingTimes;
  List<WeightPrice> weightPrice;
  List<WaterTempPrice> waterTempPrice;
  List<DryTempPrice> dryTempPrice;

  Store(
      {this.merchant_Id,
      this.shop_name,
      this.address,
      this.location,
      this.workingTimes,
      this.weightPrice,
      this.waterTempPrice,
      this.dryTempPrice});

  Store.fromJson(Map<String, dynamic> json) {
    merchant_Id = json['merchant_id'];
    shop_name = json['shop_name'];
    address = json['address'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    if (json['working_time'] != null) {
      workingTimes = new List<dynamic>();
      json['working_time'].forEach((v) {
        workingTimes.add(v);
      });
    }
    if (json['weight_price'] != null) {
      weightPrice = new List<WeightPrice>();
      json['weight_price'].forEach((v) {
        weightPrice.add(new WeightPrice.fromJson(v));
      });
    }
    if (json['water_temp_price'] != null) {
      waterTempPrice = new List<WaterTempPrice>();
      json['water_temp_price'].forEach((v) {
        waterTempPrice.add(new WaterTempPrice.fromJson(v));
      });
    }
    if (json['dry_temp_price'] != null) {
      dryTempPrice = new List<DryTempPrice>();
      json['dry_temp_price'].forEach((v) {
        dryTempPrice.add(new DryTempPrice.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_id'] = this.merchant_Id;
    data['shop_name'] = this.shop_name;
    data['address'] = this.address;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.workingTimes != null) {
      data['working_time'] = this.workingTimes.map((v) => v.toJson()).toList();
    }
    if (this.weightPrice != null) {
      data['weight_price'] = this.weightPrice.map((v) => v.toJson()).toList();
    }
    if (this.waterTempPrice != null) {
      data['water_temp_price'] =
          this.waterTempPrice.map((v) => v.toJson()).toList();
    }
    if (this.dryTempPrice != null) {
      data['dry_temp_price'] =
          this.dryTempPrice.map((v) => v.toJson()).toList();
    }
    return data;
  }

  get workingDay {
    if (this.workingTimes == null) return null;
    var date = DateTime.now();
    String time = _textSelect(this.workingTimes[date.weekday - 1].toString());
    if (time == "null") {
      return "ปิดทำการ";
    } else {
      String amPm = DateFormat('a').format(date);
      return time + " $amPm";
    }
  }

  bool openCloseStore() {
    if (this.workingTimes == null) return false;
    var date = DateTime.now();
    String time = _textSelect(this.workingTimes[date.weekday - 1].toString());

    if (time == "null") {
      return false;
    } else {
      DateTime now = DateTime.now();
      DateFormat formatter = DateFormat('yyyy-MM-dd');
      String startFormatted =
          formatter.format(now) + " " + time.substring(0, 5);
      String endFormatted = formatter.format(now) + " " + time.substring(6, 11);
      DateTime startDate = DateTime.parse(startFormatted);
      DateTime endDate = DateTime.parse(endFormatted);

      if (startDate.isBefore(now) && endDate.isAfter(now)) {
        return true;
      } else {
        return false;
      }
    }
  }

  String _textSelect(String str) {
    str = str.replaceAll(' ', '');
    str = str.replaceAll(',', '-');
    str = str.replaceAll('[', '');
    str = str.replaceAll(']', '');
    return str;
  }
}
