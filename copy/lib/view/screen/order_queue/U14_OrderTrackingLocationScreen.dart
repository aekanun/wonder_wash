import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/view/styles/StyleConst.dart';
import 'package:laundry_delivery_app/viewmodel/viewModel.dart';
import 'package:flutter_animarker/flutter_map_marker_animation.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:provider/provider.dart';

import 'U15_OrderTrackingTimeLineScreen.dart';

class OrderTrackingLocationScreen extends StatefulWidget {
  int index;
  OrderTrackingLocationScreen({Key key, this.index}) : super(key: key);
  _OrderTrackingLocationState createState() => _OrderTrackingLocationState();
}

class _OrderTrackingLocationState extends State<OrderTrackingLocationScreen> {
  OrderQueueViewModel orderQueueViewModel;
  final Completer<GoogleMapController> mapController = Completer();

  @override
  void initState() {
    super.initState();
    this.orderQueueViewModel =
        Provider.of<OrderQueueViewModel>(context, listen: false);
    orderQueueViewModel.mqttContect(widget.index);
    print(
        '==============================OrderTrackingLocation init==============================');
  }

  @override
  Future<void> dispose() async {
    print(
        '==============================OrderTrackingLocation dispose==============================');
    orderQueueViewModel.discontect();
    // var controller = await mapController.future;
    // controller.dispose();
    super.dispose();
  }

  Future<void> onStopover(LatLng latLng) async {
    if (!mapController.isCompleted) return;
  }

  Future<bool> onWillPop() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Consumer<OrderQueueViewModel>(
        builder: (context, orderQueue, _) {
          return Container(
            child: Scaffold(
              backgroundColor: ColorsConst.white,
              appBar: AppBar(
                // key: orderQueue.dto.scfdRootKey,
                title: Text(
                  "รายละเอียดการจัดส่ง",
                  style: TextStyle(color: Colors.black),
                ),
                bottomOpacity: 0.0,
                elevation: 0.0,
                backgroundColor: ColorsConst.white,
                leading: new IconButton(
                    icon: new Icon(Icons.arrow_back),
                    color: Colors.black,
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
              body: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        margin: new EdgeInsets.all(8.0),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                          child: Row(children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'ออเดอร์ ' +
                                      orderQueue.dto.orderQueue
                                          .data[widget.index].orderNo
                                          .toString(),
                                  style: TextStyleConst.b14(),
                                ),
                                Text(
                                    orderQueue.dto.orderQueue.data[widget.index]
                                        .pickupTime
                                        .toString(),
                                    style: TextStyleConst.n12()),
                              ],
                            ),
                          ]),
                        ),
                      ),
                      Container(
                        margin: new EdgeInsets.fromLTRB(10, 8, 10, 0),
                        child: Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Text(
                                    'จัดส่ง',
                                    style: TextStyleConst.b14(),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(bottom: 15),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            orderQueue
                                                .dto
                                                .orderQueue
                                                .data[widget.index]
                                                .customerDetail
                                                .name
                                                .toString(),
                                            style: TextStyleConst.n14(),
                                          ),
                                          Text(
                                            orderQueue
                                                .dto
                                                .orderQueue
                                                .data[widget.index]
                                                .customerDetail
                                                .address
                                                .toString(),
                                            style: TextStyleConst.n14(),
                                            maxLines: 2,
                                          ),
                                        ])),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: InkWell(
                                    onTap: () {
                                      orderQueue.callFromUrl(orderQueue
                                          .dto
                                          .orderQueue
                                          .data[widget.index]
                                          .merchantDetail
                                          .phone);
                                    },
                                    child: Row(children: <Widget>[
                                      Icon(Icons.phone, color: Colors.green),
                                      Flexible(
                                        child: Text(
                                            "ร้าน: " +
                                                orderQueue
                                                    .dto
                                                    .orderQueue
                                                    .data[widget.index]
                                                    .merchantDetail
                                                    .name,
                                            maxLines: 2,
                                            style: TextStyleConst.n14()),
                                      ),
                                    ]),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: InkWell(
                                    onTap: () {
                                      orderQueue.callFromUrl(orderQueue
                                          .dto
                                          .orderQueue
                                          .data[widget.index]
                                          .driverDetail
                                          .phone1
                                          .toString());
                                    },
                                    child: Row(children: <Widget>[
                                      Icon(Icons.phone, color: Colors.red),
                                      Flexible(
                                        child: Text(
                                            "ผู้รับ/ผู้ส่ง: " +
                                                orderQueue
                                                    .dto
                                                    .orderQueue
                                                    .data[widget.index]
                                                    .driverDetail
                                                    .name1,
                                            maxLines: 2,
                                            style: TextStyleConst.n14()),
                                      ),
                                    ]),
                                  ),
                                ),
                                Container(
                                  color: Colors.grey,
                                  width: double.infinity,
                                  height: 1,
                                ),
                              ],
                            )),
                      ),
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Animarker(
                          curve: Curves.ease,
                          useRotation: false,
                          zoom: orderQueue.cameraZoom,
                          onStopover: onStopover,
                          mapId: mapController.future
                              .then<int>((value) => value.mapId),
                          markers: orderQueue.dtoMqtt.markers.values.toSet(),
                          child: GoogleMap(
                            mapType: MapType.normal,
                            myLocationEnabled: true,
                            myLocationButtonEnabled: false,
                            zoomControlsEnabled: false,
                            mapToolbarEnabled: false,
                            markers: orderQueue.dto.markers,
                            initialCameraPosition: CameraPosition(
                                zoom: orderQueue.cameraZoom,
                                target: LatLng(
                                    orderQueue.dto.orderQueue.data[widget.index]
                                        .customerDetail.location.latitude,
                                    orderQueue.dto.orderQueue.data[widget.index]
                                        .customerDetail.location.longitude)),
                            onMapCreated: (mController) =>
                                mapController.complete(mController),
                          ),
                        ),
                      )),
                    ],
                  ),
                  SlidingUpPanel(
                    minHeight: 65,
                    panel: Center(
                      child: TimeLineScreen(
                          orderInfo: orderQueue.dto.orderQueueInfo),
                    ),
                    collapsed: Container(
                      decoration: BoxDecoration(
                        color: AppTheme.nearyPurple,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24.0),
                          topRight: Radius.circular(24.0),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'สถานะการจัดส่งสินค้า',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    // body: Center(child: Text('test')),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24.0),
                      topRight: Radius.circular(24.0),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
      onWillPop: onWillPop,
    );
  }
}
