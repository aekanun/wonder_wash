import 'CustomerLocation.dart';
import 'OrderDetail.dart';

class Data {
  int orderNo;
  String customerName;
  String customerAddress;
  CustomerLocation customerLocation;
  String customerPhone;
  CustomerLocation shopLocation;
  int merchantId;
  String pickupTime;
  int orderStatus;
  String driver1Name;
  String driver1Phone;
  String driver2Name;
  String driver2Phone;
  String orderFrom;
  String orderTo;
  String distance;
  String moneyPickup;
  String moneyReceive;
  List<OrderDetail> orderDetail;
  int totalCharge;
  String paymentOption;

  Data(
      {this.orderNo,
      this.customerName,
      this.customerAddress,
      this.customerLocation,
      this.customerPhone,
      this.shopLocation,
      this.merchantId,
      this.pickupTime,
      this.orderStatus,
      this.driver1Name,
      this.driver1Phone,
      this.driver2Name,
      this.driver2Phone,
      this.orderFrom,
      this.orderTo,
      this.distance,
      this.moneyPickup,
      this.moneyReceive,
      this.orderDetail,
      this.totalCharge,
      this.paymentOption});

  Data.fromJson(Map<String, dynamic> json) {
    orderNo = json['order_no'];
    customerName = json['customer_name'];
    customerAddress = json['customer_address'];
    customerLocation = json['customer_location'] != null
        ? new CustomerLocation.fromJson(json['customer_location'])
        : null;
    customerPhone = json['customer_phone'];
    shopLocation = json['shop_location'] != null
        ? new CustomerLocation.fromJson(json['shop_location'])
        : null;
    merchantId = json['merchant_id'];
    pickupTime = json['pickup_time'];
    orderStatus = json['order_status'];
    driver1Name = json['driver_1_name'];
    driver1Phone = json['driver_1_phone'];
    driver2Name = json['driver_2_name'];
    driver2Phone = json['driver_2_phone'];
    orderFrom = json['order_from'];
    orderTo = json['order_to'];
    distance = json['distance'];
    moneyPickup = json['money_pickup'];
    moneyReceive = json['money_receive'];
    if (json['order_detail'] != null) {
      orderDetail = new List<OrderDetail>();
      json['order_detail'].forEach((v) {
        orderDetail.add(new OrderDetail.fromJson(v));
      });
    }
    totalCharge = json['total_charge'];
    paymentOption = json['payment_option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_no'] = this.orderNo;
    data['customer_name'] = this.customerName;
    data['customer_address'] = this.customerAddress;
    if (this.customerLocation != null) {
      data['customer_location'] = this.customerLocation.toJson();
    }
    data['customer_phone'] = this.customerPhone;
    if (this.shopLocation != null) {
      data['shop_location'] = this.shopLocation.toJson();
    }
    data['merchant_id'] = this.merchantId;
    data['pickup_time'] = this.pickupTime;
    data['order_status'] = this.orderStatus;
    data['driver_1_name'] = this.driver1Name;
    data['driver_1_phone'] = this.driver1Phone;
    data['driver_2_name'] = this.driver2Name;
    data['driver_2_phone'] = this.driver2Phone;
    data['order_from'] = this.orderFrom;
    data['order_to'] = this.orderTo;
    data['distance'] = this.distance;
    data['money_pickup'] = this.moneyPickup;
    data['money_receive'] = this.moneyReceive;
    if (this.orderDetail != null) {
      data['order_detail'] = this.orderDetail.map((v) => v.toJson()).toList();
    }
    data['total_charge'] = this.totalCharge;
    data['payment_option'] = this.paymentOption;
    return data;
  }
}