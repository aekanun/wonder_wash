import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';
import 'package:laundry_delivery_app/utility/ScaleRoute.dart';
import 'package:laundry_delivery_app/view/screen/order/PaymentType.dart';
import 'package:laundry_delivery_app/view/styles/AppTheme.dart';
import 'package:laundry_delivery_app/view/styles/ColorsConst.dart';
import 'package:laundry_delivery_app/view/styles/StyleConst.dart';
import 'package:laundry_delivery_app/viewmodel/OrderViewModel.dart';
import 'package:provider/provider.dart';

class ConfirmOrderListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<OrderViewModel>(builder: (context, order, _) {
      return Scaffold(
        backgroundColor: ColorsConst.white,
        appBar: AppBar(
          // key: attr.scfdRootKey,
          title: Text(
            'Please check your order',
            style: TextStyle(color: Colors.black),
          ),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: ColorsConst.white,
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                //  order.updateNavigation(u10);
                Navigator.pop(context);
              }),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: order.dto.order == null ||
                      order.dto.order.orderDetail.length == 0
                  ? Center(child: Text('Empty'))
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: order.dto.order.orderDetail.length,
                      itemBuilder: (context, index) {
                        int charge;
                        charge = order.dto.order.orderDetail[index]
                                .waterTempPrice.price +
                            order.dto.order.orderDetail[index].dryTempPrice
                                .price +
                            order.dto.order.orderDetail[index].weightPrice
                                .price;
                        return Column(
                          children: <Widget>[
                            new ListTile(
                              leading: Icon(Icons.shopping_basket),
                              title: Text('Basket ' + (index + 1).toString()),
                              tileColor: Colors.blueGrey[100],
                              selectedTileColor: Colors.black54,
                            ),
                            new ListTile(
                              //leading: Icon(Icons.shopping_basket),
                              title: Text('Clothes type: '),
                              trailing: Text('Regular'),
                            ),
                            new ListTile(
                              title: Text('Clothes weight: '),
                              subtitle: Text(order.dto.order.orderDetail[index]
                                      .weightPrice.weight
                                      .toString() +
                                  " kg"),
                              trailing: Text(order.dto.order.orderDetail[index]
                                      .weightPrice.price
                                      .toString() +
                                  " ฿"),
                            ),
                            new ListTile(
                              title: Text('Water temperature: '),
                              subtitle: Text(order.dto.order.orderDetail[index]
                                  .waterTempPrice.temp
                                  .toString()),
                              trailing: Text(order.dto.order.orderDetail[index]
                                      .waterTempPrice.price
                                      .toString() +
                                  " ฿"),
                            ),
                            new ListTile(
                              title: Text('Dryer temperature: '),
                              subtitle: Text(order.dto.order.orderDetail[index]
                                  .dryTempPrice.temp
                                  .toString()),
                              trailing: Text(order.dto.order.orderDetail[index]
                                      .dryTempPrice.price
                                      .toString() +
                                  " ฿"),
                            ),
                            new ListTile(
                              title: Text('Note'),
                              subtitle:
                                  Text(order.dto.order.orderDetail[index].note),
                            ),
                            new ListTile(
                              title: Text('Charge: '),
                              trailing: Text(charge.toString() + " ฿"),
                            ),
                          ],
                        );
                      },
                    ),
            ),
            Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.purple),
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(20.0),
                      topRight: const Radius.circular(20.0),
                    )),
                child: Column(
                  children: <Widget>[
                    new CheckboxListTile(
                      title: Text("immediately"),
                      value: order.getSelectedCheckBoxImmediately,
                      onChanged: (value) {
                        order.updateSelectedCheckBoxImmediately(value);
                      },
                    ),
                    new ListTile(
                      title: Text('Select a time'),
                      subtitle: Text(order.dto.order.pickupTime),
                      trailing: Icon(Icons.edit),
                      onTap: () {
                        DatePicker.showTimePicker(context,
                            showTitleActions: true,
                            theme: DatePickerTheme(
                                headerColor: Colors.white,
                                backgroundColor: Colors.white,
                                itemStyle: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                                doneStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16)), onConfirm: (date) {
                          order.setDateTimeOrder(date);
                        },
                            currentTime: DateTime.now(),
                            locale: LocaleType.th,
                            showSecondsColumn: false);
                      },
                    ),
                    new ListTile(
                      title: Text('Payment: '),
                      subtitle: Text('order.dto.order.paymentOption'),
                      trailing: Icon(Icons.edit),
                      onTap: () {
                        Navigator.push(context,
                            ScaleRoute(page: PaymentType(viewModel: order)));
                      },
                    ),
                  ],
                ),
              ),
            ),

            // new ListTile(
            //   title: Text('Select a time'),
            //   subtitle: Text(order.dto.order.pickupTime),
            //   trailing: Icon(Icons.edit),
            //   onTap: () {
            //     DatePicker.showTimePicker(context,
            //         showTitleActions: true,
            //         theme: DatePickerTheme(
            //             headerColor: Colors.white,
            //             backgroundColor: Colors.white,
            //             itemStyle: TextStyle(
            //                 color: Colors.black,
            //                 fontWeight: FontWeight.bold,
            //                 fontSize: 18),
            //             doneStyle:
            //                 TextStyle(color: Colors.black, fontSize: 16)),
            //         onConfirm: (date) {
            //       order.setDateTimeOrder(date);
            //     },
            //         currentTime: DateTime.now(),
            //         locale: LocaleType.th,
            //         showSecondsColumn: false);
            //   },
            // ),
            // new ListTile(
            //   title: Text('Payment: '),
            //   subtitle: Text(order.dto.order.paymentOption),
            //   trailing: Icon(Icons.edit),
            //   onTap: () {
            //     Navigator.push(
            //         context, ScaleRoute(page: PaymentType(viewModel: order)));
            //   },
            // ),
            Container(
              padding: new EdgeInsets.all(8.0),
              color: AppTheme.nearyPurple.withOpacity(0.5),
              height: 48.0,
              child: Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.shopping_cart),
                        Text('Total charge'),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.only(right: 10),
                    //margin: EdgeInsets.all(10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child: Container(
                        color: Colors.white,
                        width: 80,
                        height: 40,
                        child: Center(
                          child: Text(
                              order.dto.order.totalCharge.toString() + " ฿"),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 16, bottom: 8, right: 16, top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 48,
                      height: 48,
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppTheme.nearlyWhite,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          border:
                              Border.all(color: AppTheme.grey.withOpacity(0.2)),
                        ),
                        child: Icon(
                          Icons.close,
                          color: AppTheme.nearlyRed,
                          size: 28,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: new GestureDetector(
                    onTap: () {
                      order.confirmOrder();
                      Navigator.pushNamed(context, u13);
                    },
                    child: Container(
                      height: 48.0,
                      padding: new EdgeInsets.all(8.0),
                      alignment: Alignment.center,
                      decoration: TextStyleConst.linearGradient(),
                      child: Text(
                        'Confirm',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
