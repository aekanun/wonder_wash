final String google_client_id =
    "919895893523-34f5il9n2rluhqcqb4hvibehccekuelc.apps.googleusercontent.com";

final String url_customer_register = "/customer/register";
final String url_customer_verify = "/customer/verify";
final String url_customer_login = "/customer/login";
final String url_customer_store_list = "/customer/store/list";
final String url_order_submit = "/customer/order/submit";
final String url_order_history = "/customer/order/history";
final String url_order_queue = "/customer/order/list";
//no use
final String url_customer_mqttid = "/customer/mqttid";
final String url_customer_tel_edit = "/customer/telephone/edit";
final String url_customer_otp_request = "/customer/otp/request";
final String url_customer_profile_edit = "/customer/profile/edit";

const String basic_authen = 'api';
const String facebook = 'facebook';
const String google = 'google';

//mqtt Wonderwash
dynamic connect_mqtt = {
  "broker": "git.anundatech.com",
  "port": 1883,
  "username": "wonderwash:wonderwash",
  "key": "wonderwash@dmin",
  "topic": "/wonder/"
};

// topic == /wonder/<merchant_id>/<order_id>/tracking/

//routing
const String u01 = '/';
const String u02 = '/sign_in_home';
const String u02_0 = '/signin';
const String u03 = '/user_sign_in';
const String u04 = '/user_register';
const String u05 = '/verify_phone';
const String u06 = '/otp_verification';
const String u07 = '/intro_welcome';
const String u08_0 = '/find_location';
const String u08 = '/laundry_home';
const String u09 = '/pick_location';
const String u10 = '/order_list';
const String u11 = '/order_list_view';
const String u12 = '/confirm_order';
const String u13 = '/order_request';
// const String u14 = '/order_tracking_location';
// const String u15 = '/order_tracking_timeline';
const String u16 = '/setting';
const String u17 = '/order_history';
const String u18 = '/edit_phone_number';
const String u19 = '/otp_verification_for_edit';
const String u20 = '/video_tutorial';
const String u21 = '/edit_profile';
const String u22 = '/order_queue';
