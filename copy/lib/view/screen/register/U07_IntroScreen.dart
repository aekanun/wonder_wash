import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:laundry_delivery_app/utility/ContainsService.dart';

class IntroScreen extends StatefulWidget {
  IntroScreenState createState() => IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  @override
  initState() {
    super.initState();
    print('IntroScreen initState');
  }

  Widget buildButtonTotutorial() {
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 36),
        padding: const EdgeInsets.only(top: 20.0, bottom: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Icon(
                  Icons.play_circle_fill,
                  color: Colors.purple,
                )),
            Container(
                margin: const EdgeInsets.only(left: 10.0),
                child: Text(
                  "watch tutorial",
                  style: TextStyle(fontSize: 20.0),
                ))
          ],
        ),
      ),
      onTap: () {
        Navigator.pushNamed(context, u20);
      },
    );
  }

  Widget buildButtonNext() {
    return InkWell(
      child: Container(
          alignment: Alignment.center,
          constraints: BoxConstraints.expand(height: 45),
          child: Text("Next",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, color: Colors.white)),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16), color: Colors.purple),
          margin: EdgeInsets.only(left: 26, right: 26, bottom: 36),
          padding: EdgeInsets.all(12)),
      onTap: () {
        Navigator.pushReplacementNamed(context, u08_0);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            buildButtonTotutorial(),
            Text("Welcome!",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 32, color: Colors.purple)),
            buildButtonNext(),
          ],
        ),
      ),
    );
  }
}
